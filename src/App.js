import React from "react";

import Calculadora from "./Components/Js/Calculadora";


function App() {
  return (
    <div>
      <Calculadora />
    </div>
  );
}

export default App;
