import React, { Component } from "react";

import Fila from "./Fila";

import "../Css/Calculadora.css";

export default class Calculadora extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pantalla: 0, operacion: ''
    };
    this.clicado = this.clicado.bind(this);
  }

  clicado(tecla) {
    const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    const ops = ["*", "+", "-", "/", "="];
    if (tecla.target.id === "C") {
      this.setState({
        //borramos el contenido de la pantalla
        pantalla: 0
      });
    } else if (tecla.target.id < numbers.length && tecla.target.id >= 0) {
      if (this.state.pantalla === 0) {
        this.setState({
          // concatenar tecla al contenido de la pantalla actual
          pantalla: tecla.target.id
        });
      }
      else {
        let PantallaActual = this.state.pantalla + tecla.target.id;
        this.setState({
          // concatenar tecla al contenido de la pantalla actual
          pantalla: PantallaActual
        });
      }
    } else {
      let Encontrada = false;
      for (let i = 0; i < ops.length && !Encontrada; i++) {
        if (tecla.target.id === ops[i]) {
          Encontrada = true;
          if (tecla.target.id === "=") {
            let Resultado = 0;
            switch (this.state.operacion) {
              case "*":
                Resultado = parseInt(this.state.pantalla.split("*")[0]) * parseInt(this.state.pantalla.split("*")[1]);
                break;
              case "+":
                Resultado = parseInt(this.state.pantalla.split("+")[0]) + parseInt(this.state.pantalla.split("+")[1]);
                break;
              case "-":
                Resultado = parseInt(this.state.pantalla.split("-")[0]) - parseInt(this.state.pantalla.split("-")[1]);
                break;
              case "/":
                Resultado = parseInt(this.state.pantalla.split("/")[0]) / parseInt(this.state.pantalla.split("/")[1]);
                break;

            }
            this.setState({
              // concatenar tecla al contenido de la pantalla actual
              pantalla: Resultado, operacion: ''
            });
          }
          else {
            let PantallaActual = this.state.pantalla + tecla.target.id;
            this.setState({
              // concatenar tecla al contenido de la pantalla actual
              pantalla: PantallaActual, operacion: tecla.target.id
            });
          }
        }
      }
      // tecla es =
      // recuperamos valor guardado y operación en curso,
      // realizamos operación con valor actual en pantalla
      // y mostramos el resultado...

    }
  }

  render() {
    return (
      <div>
        <div className="pantalla">
          <div className="resultado">{this.state.pantalla}</div>
        </div>
        <div>
          <Fila teclas={[1, 2, 3, "*"]} onClick={this.clicado} />
          <Fila teclas={[4, 5, 6, "/"]} onClick={this.clicado} />
          <Fila teclas={[7, 8, 9, "-"]} onClick={this.clicado} />
          <Fila teclas={[0, "C", "=", "+"]} onClick={this.clicado} />
        </div>
      </div>
    );
  }
}
